# upgrade brew, yarn, sass and dependencies

echo "brew gets updated..."
brew update

echo "yarn gets upgraded..."
brew upgrade yarn

echo "sass gets upgraded..."
brew upgrade sass

echo "Caution: dependencies get upgraded potentially across major versions"
yarn upgrade --latest

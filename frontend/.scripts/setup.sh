# install yarn and sass to manage the frontend dependencies

echo "yarn gets installed..."
brew install yarn

echo "sass gets installed..."
brew install sass/sass/sass

echo "gulp gets installed..."
yarn global add gulp

echo "mysql gets installed..."
brew install mysql

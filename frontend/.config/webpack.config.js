const path = require("path")

module.exports = {
  output: {
    filename: '[name].bundle.js',
    path: path.join(__dirname, "../dist/assets/js/")
  },
  mode: 'development',
  module: {
    rules: [{
      test: /\.js$/,
      use: [{
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env', ]
        }
      }]
    }]
  },
}

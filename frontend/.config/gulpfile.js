const autoprefixer = require('autoprefixer');
const concat = require('gulp-concat');
const cssImport = require('postcss-import')
const cssnano = require('cssnano');
const gulp = require('gulp');
const postcss = require('gulp-postcss');
const pxtorem = require('postcss-pxtorem')
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');
const webpackStream = require('webpack-stream');
const webpackConfig = require('./webpack.config.js');
const named = require('vinyl-named');

const config = {
  path: {
    src: '../src/assets/',
    dist: '../dist/assets/',
    modules: '../node_modules/'
  }
}

// CSS
function compilecss() {
  return gulp
    .src([config.path.dist + 'css/!(*.min).css'])
    .pipe(postcss([
      autoprefixer(),
      cssImport(),
      pxtorem({
        rootValue: 16,
        unitPrecision: 5,
        propWhiteList: ['font', 'font-size', 'line-height', 'letter-spacing'],
        replace: true,
        mediaQuery: false,
        minPixelValue: 2
      }),
      cssnano({
        zindex: false
      })
    ]))
    .pipe(rename({
      suffix: ".min"
    }))
    .pipe(gulp.dest(config.path.dist + 'css/'))
}

function css() {
  return gulp
    .src(config.path.src + 'scss/**.scss')
    .pipe(sass({
      includePaths: [config.path.modules, config.path.src + 'scss'],
      outputStyle: 'expanded'
    }).on('error', sass.logError))
    .pipe(gulp.dest(config.path.dist + 'css/'))
}

function grid() {
  return gulp
    .src(config.path.src + 'scss/utils/_grid.scss')
    .pipe(concat('grid.scss'))
    .pipe(sass({
      includePaths: [config.path.modules, config.path.src + 'scss'],
      outputStyle: 'expanded'
    }).on('error', sass.logError))
    .pipe(gulp.dest(config.path.dist + 'css/'))
}


// Javascript
function js() {
  return gulp
    .src([config.path.src + 'js/*.js'])
    .pipe(named())
    .pipe(webpackStream(webpackConfig))
    .pipe(gulp.dest(config.path.dist + 'js/'))
    .pipe(uglify())
    .pipe(rename({
      suffix: ".min"
    }))
    .pipe(gulp.dest(config.path.dist + 'js/'))
}

// Copy other files (images, fonts, etc.)
function copyFiles() {
  return gulp
    .src([config.path.src + '**/*', '!/**/*.json', '!' + config.path.src + 'scss/**', '!' + config.path.src + 'js/features/**'], {
      nodir: true
    })
    .pipe(gulp.dest(config.path.dist))
}

// Watch files
function watchFiles() {
  gulp.watch([config.path.src + 'scss/utils/_grid.scss'], {ignoreInitial: false}, gulp.series(grid, compilecss));
  gulp.watch([config.path.src + 'scss/', config.path.src + 'shared-variables.json', '!' + config.path.src + 'scss/utils/_grid'], {ignoreInitial: false}, gulp.series(css, compilecss));
  gulp.watch([config.path.src + 'js/', config.path.src + 'shared-variables.json'], {ignoreInitial: false}, js);
  gulp.watch([config.path.src + '**/*', '!' + config.path.src + 'js/features/**', '!' + config.path.src + 'scss/**', '!' + config.path.src + 'shared-variables.json'], {ignoreInitial: false}, copyFiles);
}


exports.grid = grid;
exports.css = css;
exports.compilecss = compilecss;
exports.js = js;
exports.copy = copyFiles;
exports.watch = watchFiles;

exports.default = gulp.parallel(gulp.series(grid, css, compilecss), js, copyFiles);

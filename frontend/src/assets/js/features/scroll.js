import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import Scrollbar from "smooth-scrollbar";

gsap.registerPlugin(ScrollTrigger);

/**
 * Smooth scroll setup
 */
const scroller = document.querySelector("[data-scroll-container]");

const scrollBar = Scrollbar.init(scroller, {
  damping: 0.1,
  alwaysShowTracks: false,
  continuousScrolling: false,
});

const nestedScrollers = gsap.utils.toArray(".team__overlay");

nestedScrollers.forEach((nestedScroller) => {
  const nestedScrollBar = Scrollbar.init(nestedScroller, {
    damping: 0.1,
    alwaysShowTracks: false,
    continuousScrolling: false,
  });
});

ScrollTrigger.scrollerProxy(scroller, {
  scrollTop(value) {
    if (arguments.length) {
      scrollBar.scrollTop = value;
    }
    return scrollBar.scrollTop;
  },
});

ScrollTrigger.defaults({ scroller: scroller });
scrollBar.addListener(ScrollTrigger.update);

/**
 * Scroll to init
 */

window.onload = (event) => {
  const queryString = window.location.search;

  if (queryString) {
    const urlParams = new URLSearchParams(queryString);
    const scrollParam = urlParams.get("scrollTo");
    const scrollTarget = document.querySelector(scrollParam);
    scrollBar.scrollIntoView(scrollTarget, {
      alignToTop: true,
      onlyScrollIfNeeded: false,
    });
    history.replaceState(
      null,
      "",
      window.location.pathname +
        window.location.search
          .replace(/[\?&]scrollTo=[^&]+/, "")
          .replace(/^&/, "?")
    );
  }

  scrollTo("[data-scrollto]");
};

function scrollTo(target) {
  const clickTarget = document.querySelectorAll(target);
  const isHome = document.body.classList.contains("home-page");

  for (var el of clickTarget) {
    const scrollTarget = document.querySelector(el.dataset.scrollto);
    const scrollLink = el.dataset.scrolllink;

    const href = el.getAttribute("href");
    el.addEventListener("click", (event) => {
      event.preventDefault();
      if (scrollLink && !isHome) {
        window.location.href = href + "?scrollTo=" + scrollTarget;
      } else {
        scrollBar.scrollIntoView(scrollTarget, {
          alignToTop: true,
          onlyScrollIfNeeded: false,
        });
      }
    });
  }
}

// Prevent scrolling of Navbar
function preventScroll(e) {
  e.preventDefault();
  e.stopPropagation();

  return false;
}
document.querySelector(".navbar").addEventListener("wheel", preventScroll);
document.querySelector(".navbar").addEventListener("touch", preventScroll);
document.querySelector(".navbar").addEventListener("scroll", preventScroll);

window.addEventListener("load", function () {
  setTimeout(function () {
    // This hides the address bar:
    window.scrollTo(0, 20);
  }, 0);
});

addEventListener("DOMContentLoaded", () => {
  let overlay = document.querySelector(".overlay__wrapper");

  if (overlay) {
    setTimeout(function () {
      overlay.classList.add("overlay--visible");
    }, 3000);

    let closebtn = overlay.querySelector(".overlay__closebtn");

    closebtn.addEventListener("click", () => {
      overlay.classList.remove("overlay--visible");
    });
  }
});

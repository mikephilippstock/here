import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);

/**
 * Multiple from side in-scrolling text rows
 */
const scrollTextRows = document.querySelectorAll(".scrolltext__entry");

scrollTextRows.forEach((textRow, index) => {
  let tl = gsap.timeline({
    scrollTrigger: {
      trigger: ".module--scrolltext",
      start: "top bottom",
      end: "top -100%",
      scrub: 1.5,
    },
  });
  tl.fromTo(
    textRow,
    {
      x: index % 2 == 0 ? "-100%" : "100%",
    },
    {
      x: "0%",
      ease: "none",
    }
  );
  tl.to(textRow, {
    x: index % 2 == 0 ? "100%" : "-100%",
    ease: "none",
  });
});

/**
 * Sticky scroll gallery on desktop
 */
const teaserItems = gsap.utils.toArray(".teaser__item");
const teaserList = document.querySelector(".teaser__list");

if (teaserItems && teaserList) {
  ScrollTrigger.matchMedia({
    "(min-width: 1024px)": function () {
      ScrollTrigger.create({
        trigger: ".teaser",
        start: "top top",
        end: "bottom bottom",
        pin: ".teaser__title",
        pinType: "transform",
        // snap: 1 / (teaserItems.length - 1),
        onUpdate: ({ progress }) => {
          let scrollItem = Math.round(progress * (teaserItems.length - 1));
          teaserList
            .querySelector(".teaser__list-item--active")
            .classList.remove("teaser__list-item--active");
          teaserList
            .querySelectorAll("li")
            [scrollItem].classList.add("teaser__list-item--active");
        },
      });
      ScrollTrigger.create({
        trigger: ".teaser",
        start: "top top",
        end: "bottom bottom",
        pin: ".teaser__info",
        pinType: "transform",
      });
    },
  });
}

/**
 * Endless marquee
 */
const marqueeModules = document.querySelectorAll(".marquee");

marqueeModules.forEach((marquee, index) => {
  let tween = gsap
    .to(".marquee__part", {
      xPercent: -100,
      repeat: -1,
      duration: 30,
      ease: "linear",
    })
    .totalProgress(0.5);

  gsap.set(".marquee", { xPercent: -50 });
});

/**
 * "Back to top" button
 */
const arrowButton = document.querySelectorAll(".navbar__arrow");
if (arrowButton) {
  gsap.fromTo(
    arrowButton,
    {
      opacity: 0,
    },
    {
      opacity: 1,
      scrollTrigger: {
        trigger: ".footer",
        start: "center bottom",
        end: "center bottom",
        scrub: 1,
      },
    }
  );
}

/**
 * Lighten intro on scroll
 */
const introImage = document.querySelector(".intro__image");

if (introImage) {
  const introText = document.querySelector(".intro__text");
  gsap.to([introImage], {
    opacity: 1,
    scrollTrigger: {
      trigger: ".main",
      start: "top bottom",
      end: "15% bottom",
      scrub: 0.5,
    },
  });
  gsap.to([introText], {
    opacity: 0,
    scrollTrigger: {
      trigger: ".main",
      start: "top bottom",
      end: "7.5% bottom",
      scrub: 0.5,
    },
  });
}

/**
 * Appear animation on scroll on all modules
 */
const modules = gsap.utils.toArray(".module:not(.module--unanimated)");

modules.forEach((module) => {
  gsap.fromTo(
    module,
    {
      opacity: 0,
      y: 100,
    },
    {
      opacity: 1,
      y: 0,
      duration: 0.5,
      scrollTrigger: {
        trigger: module,
        toggleActions: "play none none none",
      },
    }
  );
});

/**
 * Show navbar on home after intro
 */
const homePage = document.querySelector(".home-page");

if (homePage) {
  const navBar = document.querySelectorAll(".navbar");

  ScrollTrigger.matchMedia({
    "(min-width: 1024px)": function () {
      gsap.fromTo(
        navBar,
        {
          x: "100%",
          y: 0,
        },
        {
          x: 0,
          y: 0,
          duration: 0.5,
          scrollTrigger: {
            trigger: navBar,
            start: window.innerHeight * 1.5 + " bottom",
            scroller: "[data-scroll-container]",
            toggleActions: "play none none none",
          },
        }
      );
    },
    "(max-width: 1023px)": function () {
      gsap.fromTo(
        navBar,
        {
          x: 0,
          y: "-100%",
        },
        {
          x: 0,
          y: 0,
          duration: 0.5,
          scrollTrigger: {
            trigger: navBar,
            start: window.innerHeight * 1.9 + " bottom",
            scroller: "[data-scroll-container]",
            toggleActions: "play none none none",
          },
        }
      );
    },
  });
}

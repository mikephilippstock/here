const navTriggers = document.querySelectorAll(".js-nav-trigger");

function initNav() {
  navTriggers.forEach((navTrigger) => {
    navTrigger.addEventListener("click", toggleNav);
  });
}

function removeAnchorTag(url) {
  const anchorIndex = url.indexOf("#");
  if (anchorIndex !== -1) {
    return url.substring(0, anchorIndex);
  }
  return url;
}

function toggleNav() {
  // close nav or modal depending on what's active
  if (document.body.classList.contains("js-overlay-active")) {
    document
      .querySelector(".team__overlay-outer--active")
      .classList.remove("team__overlay-outer--active");
    document.body.classList.remove("js-overlay-active");
    history.pushState(null, null, removeAnchorTag(window.location.href));
  } else {
    document.body.classList.toggle("js-nav-active");
  }
}

if (navTriggers) {
  initNav();
}

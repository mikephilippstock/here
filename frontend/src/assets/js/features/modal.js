const modals = document.querySelectorAll(".team__overlay-outer");

if (modals) {
  modals.forEach((modal) => {
    const trigger = modal.dataset.trigger;
    const triggerEl = document.querySelector(`[data-modal=${trigger}]`);

    triggerEl.addEventListener("click", (e) => {
      e.preventDefault();
      modal.classList.add("team__overlay-outer--active");
      document.body.classList.add("js-overlay-active");
      history.pushState(
        null,
        null,
        location.href.replace(location.hash, "") + "#" + trigger
      );
    });
  });

  const urlHash = window.location.hash.substring(1);
  if (urlHash) {
    const modalToOpen = document.querySelector(`[data-modal=${urlHash}]`);
    modalToOpen.click();
  }
}
